var timeDelimiter = ".";
var nbsp = " ";

function parseTime(s)
{
	if (s == nbsp + nbsp + "." + nbsp + nbsp) return { "h": 0, "m": 0};

	var time = s.split(timeDelimiter);

	return { "h": parseInt(time[0]), "m": parseInt(time[1])};
}

function parseTimeAsMinutes(s)
{
	var parsed = parseTime(s);
	return parsed.h * 60 + parsed.m * 1;
}

function formatMinutesAsTime(m)
{
	var h = Math.trunc(Math.abs(m) / 60);
	var mm = Math.abs(m) - h * 60;

	return (m < 0 ? "-" : nbsp) + leadingZeros(h) + timeDelimiter + leadingZeros(mm); 
}

function leadingZeros(num)
{
	var s = "00" + num;
	return s.substr(s.length - 2);
}

function createSpan(h, m, c)
{
	return createSpanFormatted(leadingZeros(h) + ":" + leadingZeros(m), c);
}

function createSpanFormatted(s, c)
{
	var el = document.createElement("span");
	el.textContent = s;
	el.style.color = c;

	return el;
}

// function randomInput()
// {
// 	var t = document.getElementById("pronet");

// 	for (var x = 0; x < 10; x++)
// 	{
// 		var tr = document.createElement("tr");
// 		tr.className = "item";

// 		var h = 7 + (Math.trunc(Math.random() * 10000) % 3);
// 		var m = Math.trunc(Math.random() * 10000) % 60;


// 		tr.appendChild(createSpan(8, 0, "todo"));
// 		tr.appendChild(createSpan(h, m, "done"));
		
// 		t.appendChild(tr);
// 	}
// }

function run()
{
	var table = document.getElementById("G_timesheetxTimesheetGrid");
	var tbody = table.getElementsByTagName("tbody")[0];
	var rows = tbody.getElementsByTagName("tr");

	var sumToDo = 0;
	var sumDone = 0;

	for (var i = 0; i < rows.length; i++)
	{
		var row = rows[i];
		var cols = row.getElementsByTagName("td");
		
		var todo = parseTimeAsMinutes(cols[3].textContent);
		var done = parseTimeAsMinutes(cols[5].textContent);

		sumDone += done;
		sumToDo += todo;

		if (todo == 0) continue;
		if (done == 0) break;

		var diff = done - todo;
		var sumDiff = sumDone - sumToDo;

		cols[5].appendChild(createSpanFormatted(formatMinutesAsTime(diff), diff >= 0 ? "green" : "red"));
		cols[5].appendChild(createSpanFormatted(formatMinutesAsTime(sumDiff), sumDiff >= 0 ? "green" : "red"));
	}
}

run();